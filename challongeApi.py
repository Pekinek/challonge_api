import json
import requests
from requests.auth import HTTPBasicAuth
import sys
import os
import urllib
import re
import pyrebase

# F1 scoring system
rank_to_score = {
    1: 25,
    2: 18,
    3: 15,
    4: 12,
    5: 10,
    6: 8,
    7: 6,
    8: 4,
    9: 2,
    10: 1,
    11: 1,
    12: 1,
    13: 1,
    14: 1,
    15: 1,
    16: 1,
    17: 1,
    18: 1,
    19: 1,
    20: 1,
}

missing_ranks_by_participant_id = {
    68082339: 5,
    68129927: 6,
    68082270: 7,
    68090586: 8,
    68127788: 9,
    68084726: 10,
    68081932: 11,
    68081931: 12,
    68090850: 13,
}

firebase_config = {
    "apiKey": "AIzaSyB35ElGEVMRokMOXjso2Wpvzh3epRQomqk",
    "authDomain": "challongeproject-4ebbe.firebaseapp.com",
    "databaseURL": "https://challongeproject-4ebbe.firebaseio.com",
    "projectId": "challongeproject-4ebbe",
    "storageBucket": "challongeproject-4ebbe.appspot.com",
    "messagingSenderId": "198324419356"
};


def send_data(players_array, season_name, ongoing_tournaments):
    firebase = pyrebase.initialize_app(firebase_config)
    database_password = os.environ['DATABASE_PASSWORD']
    auth = firebase.auth()
    user = auth.sign_in_with_email_and_password("challonge@script.com", database_password)
    
    firebase.database().child('atos').child('completed').child(season_name).set(players_array, user['idToken'])
    if ongoing_tournaments:
        firebase.database().child('atos').child('ongoing').set(ongoing_tournaments, user['idToken'])


def get_name(name):
    non_ascii_count = len(re.findall(r'[^\x00-\x7f]', name))
    return '{:11}'.format(name.encode('utf8')) + ' ' * non_ascii_count

user = os.environ['USER']
password = os.environ['PASSWORD']
season_name = os.environ['SEASON']
created_after = os.getenv('CREATED_AFTER', '2017-01-01')
created_before = os.getenv('CREATED_BEFORE', '2018-01-01')

print('Squash season {} summary:'.format(season_name))
parameters = urllib.urlencode({
    'created_after': created_after,
    'created_before': created_before,
    'subdomain': 'squash-team-atos',
})
httpbasic_auth = HTTPBasicAuth(user, password)
r = requests.get('https://api.challonge.com/v1/tournaments.json?' + parameters, auth=httpbasic_auth)
tournaments = json.loads(r.text)
great_players_dict = {}
ongoing_tournaments = []
for t in tournaments:
    if t['tournament']['state'] != 'complete':
        ongoing_tournaments.append({
                                    'name': t['tournament']['name'],
                                    'sign_up_url': t['tournament']['sign_up_url'],
                                    })
    else:
        tournament_name = t['tournament']['name']
        r2 = requests.get(
            'https://api.challonge.com/v1/tournaments/{}.json?include_participants=1'.format(t['tournament']['id']),
            auth=httpbasic_auth)
        single_tournament = json.loads(r2.text)
        for p in single_tournament['tournament']['participants']:
            participant = p['participant']
            p_name = (participant['username'] if participant['username'] else participant['name']).upper()
            p_rank = participant['final_rank']
            if p_name not in great_players_dict:
                great_players_dict[p_name] = {
                    'player_tournaments': [],
                    'season_score': 0,
                    'display_name': participant['name'] if participant['name'] else participant['username'],
                    'avatar': participant['attached_participatable_portrait_url'],
                }
            # Hack to fill missing ranks from 5th tournament
            if (not p_rank and single_tournament['tournament']['id'] == 4217047):
                p_rank = missing_ranks_by_participant_id[participant['id']]
            if p_rank in rank_to_score:
                great_players_dict[p_name]['season_score'] += rank_to_score[p_rank]
            player_tournaments = great_players_dict[p_name]['player_tournaments']
            player_tournaments.append((tournament_name, p_rank))

sorted_keys = sorted(great_players_dict.keys(), key=lambda y: (great_players_dict[y]['season_score']), reverse=True)
sorted_array = [(k, great_players_dict[k]) for k in sorted_keys]

previous_score = 999
previous_rank = 0
table_description = "{:5s}  {:11s} {:5s} {:11s}".format('Rank', 'Name', 'Score', 'Tournaments')
print(table_description)
print('=' * len(table_description))
for i, great_player in enumerate(sorted_keys):
    display_name = great_players_dict[great_player]['display_name']
    score = great_players_dict[great_player]['season_score']
    rank = i + 1 if score < previous_score else previous_rank
    tournament_count = len(great_players_dict[great_player]['player_tournaments'])
    player_message = "{:>4d}. {:12s} {:>5d} {:>11d}".format(rank, get_name(display_name), score, tournament_count)
    print(player_message)
    previous_score = score
    previous_rank = rank

print('Congratulations!')

should_data_be_send = os.getenv('SEND_DATA', 'false')
if (should_data_be_send == 'true'):
    send_data(sorted_array, season_name, ongoing_tournaments)
